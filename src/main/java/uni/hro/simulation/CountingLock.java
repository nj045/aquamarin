package uni.hro.simulation;

import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by xor on 5/22/17.
 */
public class CountingLock {
    private Semaphore semaphore = new Semaphore(1, true);
    private AtomicInteger atomicInteger = new AtomicInteger(0);

    public synchronized CountingLock lock() {
        if (atomicInteger.incrementAndGet() > 1) {
            try {
                semaphore.acquire();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return this;
    }

    public synchronized CountingLock unlock() {
        if (atomicInteger.decrementAndGet() < 2) {
            semaphore.release();
        }
        return this;
    }
}
