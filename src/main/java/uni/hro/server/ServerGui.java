package uni.hro.server;

import java.awt.Color;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.net.InetAddress;

import net.miginfocom.swing.MigLayout;
import java.awt.Dimension;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import java.awt.FlowLayout;
import java.awt.ComponentOrientation;
import java.awt.Font;
import java.util.Properties;
import javax.swing.JLabel;

/**
 *
 * @author Johann
 *
 *	GUI of the server.
 *
 */
public class ServerGui extends JFrame {

	private static GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
	private JPanel contentPane;
	private JTextField ClientField;
	private JButton startButton;
	private JButton exitButton;
	private JScrollPane scrollPane;
	private JPanel panel;
	private JPanel numberPanel;
	private JLabel lblNumberOfClients;
	private JLabel numLabel;

	private int noC = 0; // Number of Clients connected

	/**
	 * Create the frame.
	 */
	public ServerGui() {
		setMinimumSize(new Dimension(479, 319));
		setTitle("Aquarium Server");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 228);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new MigLayout("", "[150px:n:150px][150px:n:150px,grow][150px:n:150px]", "[30px:n:30px][30px:n:30px,grow][50px:n:50px,grow][]"));

		scrollPane = new JScrollPane();
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
		contentPane.add(scrollPane, "cell 0 0 3 2,grow");

		panel = new JPanel();
		scrollPane.setViewportView(panel);

		ClientField = addClient();

		numberPanel = new JPanel();
		contentPane.add(numberPanel, "cell 0 2 3 1,alignx center,aligny center");

		lblNumberOfClients = new JLabel("Number of Clients:");
		lblNumberOfClients.setFont(new Font("Tahoma", Font.BOLD, 16));
		numberPanel.add(lblNumberOfClients);

		numLabel = new JLabel("0");
		numLabel.setFont(new Font("Tahoma", Font.PLAIN, 16));
		numberPanel.add(numLabel);

		exitButton = new JButton("Exit");
		exitButton.setPreferredSize(new Dimension(144, 23));
		exitButton.setMinimumSize(new Dimension(144, 23));
		exitButton.setMaximumSize(new Dimension(144, 23));
		contentPane.add(exitButton, "cell 0 3,alignx center,aligny center");

		startButton = new JButton("Start");
		startButton.setMaximumSize(new Dimension(144, 23));
		startButton.setMinimumSize(new Dimension(144, 23));
		startButton.setPreferredSize(new Dimension(144, 23));
		contentPane.add(startButton, "cell 2 3,alignx center,aligny center");

		// make it fullscreen
	// if on pi .....
		String os = "os.name";

		Properties prop = System.getProperties( );
		System.out.println( prop.getProperty( os ) );

		// if this is run on an Raspberry, it goes to fullscreen and only to a tiny window if not
		if ("4.4.50-v7+".equals(System.getProperty("os.version"))) {
			setResizable(false);
			setUndecorated(true); // deletes the frame decoration (the upper bar of the frame)
			gd.setFullScreenWindow(this); // GraphicsEnvironment is essential to create a fullscreen application in unix
		} else {
			setResizable(true);
			setUndecorated(false);
		}
		setVisible(true);

		// Listener for the buttons.
		exitButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				System.exit(0);
			}
		});
	}	// end of constructor

	public static void main(String []args) {new ServerGui();}

	/**
	 *
	 * @param ipAddress IP-Address of the connected client
	 */

	public void connectClient(String ipAddress) {
		noC++;
		ClientField.setText(noC + ": " + ipAddress);
		ClientField.setBackground(new Color(0, 255, 0));
		ClientField.setToolTipText(ipAddress);
		ClientField = addClient();
		numLabel.setText(""+noC);
	}

	private JTextField addClient() {
		panel.setLayout(new FlowLayout(FlowLayout.LEFT, 10, 10));
		JTextField textField = new JTextField();
		textField.setFont(new Font("Tahoma", Font.PLAIN, 20));
		textField.setAlignmentY(1.0f);
		textField.setAlignmentX(1.0f);
		textField.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		textField.setColumns(11);
		panel.add(textField);
		textField.setHorizontalAlignment(SwingConstants.CENTER);
		textField.setText("Waiting for Clients...");
		textField.setEnabled(false);
		textField.setEditable(false);
		panel.revalidate();
		panel.repaint();
		return textField;
	}

	public void setStartListener(ActionListener l) {
		this.startButton.addActionListener(l);
	}
}