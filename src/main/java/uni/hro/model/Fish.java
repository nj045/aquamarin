package uni.hro.model;

import uni.hro.model.neurons.FishInputNeuron;

import java.awt.Color;

import uni.hro.model.neurons.DeadFishInputNeuron;

/**
 * Created by Er Bü on 15.04.2017.
 */
public class Fish extends Creature {

	double suck;
	double bite;

    public Fish(World world, float x, float y) {
        super(world, 100f, x, y, 30, 13);
        createNeuron(1, 0);
        createNeuron(1, 1);
        createNeuron(1, -1);
        createNeuron(2, 0);
        createNeuron(2, 1);
        createNeuron(2, 2);
        createNeuron(2, -1);
        createNeuron(2, -2);
        createNeuron(3, 0);
        createNeuron(3, 1);
        createNeuron(3, 2);
        createNeuron(3, 3);
        createNeuron(3, -1);
        createNeuron(3, -2);
        createNeuron(3, -3);
        createNeuron(4,0);
        createNeuron(4,1);
        createNeuron(4,2);
        createNeuron(4,3);
        createNeuron(4,4);
        createNeuron(4,-1);
        createNeuron(4,-2);
        createNeuron(4,-3);
        createNeuron(4,-4);
        createNeuron(5,0);
        createNeuron(5,1);
        createNeuron(5,2);
        createNeuron(5,3);
        createNeuron(5,4);
        createNeuron(5,-1);
        createNeuron(5,-2);
        createNeuron(5,-3);
        createNeuron(5,-4);
        createNeuron(0, 1);
        createNeuron(0, -1);
        createNeuron(-1, 0);
        createNeuron(-1, -1);
        createNeuron(-1, 1);
        createSecondaryNeurons();
        createHappinessNeurons();
    }

    @Override
    public void createHappinessNeurons() {
        final int idOfFirstHappinessNeuron = neurons.size();
        createNeuron(-2, 0);
        createNeuron( 2, 0);
        createNeuron(-1, 0);
        createNeuron( 1, 0);
        createNeuron( 0,-2);
        createNeuron( 0,-1);
        happinessNeurons.add(new FishInputNeuron(neurons.get(idOfFirstHappinessNeuron +0).neuron, Fish.class));
        happinessNeurons.add(new FishInputNeuron(neurons.get(idOfFirstHappinessNeuron +1).neuron, Fish.class));
        happinessNeurons.add(new FishInputNeuron(neurons.get(idOfFirstHappinessNeuron +2).neuron, Fish.class));
        happinessNeurons.add(new FishInputNeuron(neurons.get(idOfFirstHappinessNeuron +3).neuron, Fish.class));
        happinessNeurons.add(new FishInputNeuron(neurons.get(idOfFirstHappinessNeuron +4).neuron, Fish.class));
        happinessNeurons.add(new FishInputNeuron(neurons.get(idOfFirstHappinessNeuron +5).neuron, Fish.class));
        happinessNeurons.add(new DeadFishInputNeuron(neurons.get(idOfFirstHappinessNeuron +4).neuron, Fish.class));
        happinessNeurons.add(new DeadFishInputNeuron(neurons.get(idOfFirstHappinessNeuron +5).neuron, Fish.class));
    }

    @Override
    public void determineCurrentHappiness() {
        // It makes fish happy to have fellow fish next to themselves:
        double currentHappiness = happinessNeurons.get(0).getValue()
                                + happinessNeurons.get(1).getValue();

        // However, fish which are too close or above them, make them feel uncomfortable:
        currentHappiness -= 0.5 * happinessNeurons.get(2).getValue()
                          + 0.5 * happinessNeurons.get(3).getValue()
                          + happinessNeurons.get(4).getValue()
                          + happinessNeurons.get(5).getValue();

        // Seeing a dead fish above them, scares them like shit:
        currentHappiness -= 3.0 * happinessNeurons.get(6).getValue();
        currentHappiness -= 3.0 * happinessNeurons.get(7).getValue();
        cumulativeHappiness += currentHappiness;
    }
    
    public void bite(double factor){
    	bite = factor;
    }
    
    public void suck(double factor){
    	suck = factor;
    }

    @Override
    public Color getCurrentColor() {
        int colorVal = Math.max(Math.min(255, 128 + (int) (fuel * 2f)), 128);
        final Color green = new Color(0, colorVal, 0);
        final Color red = new Color(colorVal, 0, 0);
        if(bite < suck)  // red fish eat red food
          return green;
        return red;
    }

    public static Integer calcInputSize(){
        Fish fish = new Fish(new World(1,1),1,1);
        return Creature.calcInputSize(fish);
    }

    public Fish() {

    }

    @Override
    public void handleCollisions() {
        for (WorldEntity worldEntity : collidedEntities) {
            if (worldEntity instanceof FoodSort) {
                FoodSort food = (FoodSort) worldEntity;
                if(bite > suck){
                	addFuel(food.consume());
                }
            }
            else if (worldEntity instanceof Food){  
            	Food food = (Food) worldEntity;
            	if(bite < suck){
            		addFuel(food.consume());
            	}
            }
        }
    }


    @Override
    public synchronized float consume() {
        if (!consumed) {
            consumed = true;
            if (isAlive())
                return fuel;
            return 25f;
        }
        return 0f;
    }
}
