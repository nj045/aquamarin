package uni.hro.model;

/**
 * Created by xor on 5/13/17.
 */
public class IntegerPoint extends Point<Integer> {
    public IntegerPoint(Integer x, Integer y) {
        super(x, y);
    }

    public IntegerPoint(){

    }
}
