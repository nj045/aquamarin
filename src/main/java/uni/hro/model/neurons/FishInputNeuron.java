package uni.hro.model.neurons;

import uni.hro.model.Fish;
import uni.hro.model.WorldEntity;

/**
 * Created by xor on 5/23/17.
 */
public class FishInputNeuron extends SecondaryNeuron<Fish> {

    public FishInputNeuron(InputNeuron inputNeuron, Class<Fish> clazz) {
        super(inputNeuron, clazz);
    }

    @Override
    public double getValue() {
       for (WorldEntity worldEntity : inputNeuron.getPerceivedEntities()) {
           if (worldEntity.getClass().equals(Fish.class))
               if( ((Fish) worldEntity).isAlive() )
                   return 1.0;
        }
        return 0;
    }
}
