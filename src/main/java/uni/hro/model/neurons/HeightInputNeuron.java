package uni.hro.model.neurons;

import uni.hro.model.Creature;
import uni.hro.model.Fish;

public class HeightInputNeuron extends SecondaryNeuron<Fish> {
    private Creature creature;

    public HeightInputNeuron(Creature c) {
        super(null, Fish.class);
        creature = c;
    }

    @Override
    public double getValue() {
        float y = creature.getY();
        return 1.0f - (y * 0.001f);
    }

}
