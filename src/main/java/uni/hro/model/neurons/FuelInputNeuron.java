package uni.hro.model.neurons;

import uni.hro.model.Creature;
import uni.hro.model.Fish;

public class FuelInputNeuron extends SecondaryNeuron<Fish> {
    private Creature creature;

    public FuelInputNeuron(Creature c) {
        super(null, Fish.class);
        creature = c;
    }

    @Override
    public double getValue() {
        return creature.getFuel() * 0.001f;
    }
}
