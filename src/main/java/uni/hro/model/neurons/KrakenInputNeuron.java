package uni.hro.model.neurons;

import uni.hro.model.Kraken;

/**
 * Created by xor on 5/23/17.
 */
public class KrakenInputNeuron extends SecondaryNeuron<Kraken> {

    public KrakenInputNeuron(InputNeuron inputNeuron, Class<Kraken> clazz) {
        super(inputNeuron, clazz);
    }
}
