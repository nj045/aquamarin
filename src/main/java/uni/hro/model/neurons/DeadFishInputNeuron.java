package uni.hro.model.neurons;

import uni.hro.model.Fish;
import uni.hro.model.WorldEntity;

public class DeadFishInputNeuron extends SecondaryNeuron<Fish> {
	
	public DeadFishInputNeuron(InputNeuron inputNeuron, Class<Fish> clazz) {
        super(inputNeuron, clazz);
    }

	@Override
    public double getValue() {
        for (WorldEntity worldEntity : inputNeuron.getPerceivedEntities()) {
            if (worldEntity.getClass().equals(Fish.class))
                if( !((Fish) worldEntity).isAlive() )  // Here is the difference to FishInputNeuron
                    return 1.0;
        }
        return 0;
    }
}
