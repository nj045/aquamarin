package uni.hro.model.neurons;

import uni.hro.model.Point;

public class InputNeuronWithPosition {
    public InputNeuron neuron;
    public Point<Integer> point;
    
    public InputNeuronWithPosition(InputNeuron n, Point<Integer> p) {
        neuron = n;
        point = p;
    }
}
