package uni.hro.model;

import java.util.Random;

public class FoodSort extends Food {

	public FoodSort(World world, float x, float y) {
		super(world, x, y);
		
		//this.sinkspeed = ((double)generateRandom(2, 7))/10; in Food.java aktiviert
		super.sinkspeed=0.3;
		super.fuel=25;
	}

    /*
	public static int generateRandom(int min, int max) {
		Random rn = new Random();
		int randomvalue = rn.nextInt(max - min + 1) + min;
		return randomvalue;
	}
	*/

}
