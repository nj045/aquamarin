package uni.hro.model;

import org.jgap.Chromosome;
import uni.hro.model.neurons.*;

import java.util.*;


public abstract class Creature extends Consumable {

	protected float fuel;
	protected World world;
	protected ArrayList<InputNeuronWithPosition> neurons = new ArrayList<>();
	protected boolean looksLeft = false;
	protected List<SecondaryNeuron<Fish>> fishInputs = new ArrayList<>();
	protected List<SecondaryNeuron<Fish>> deadFishInputs = new ArrayList<>();
	protected List<SecondaryNeuron<Kraken>> krakenInputs = new ArrayList<>();
	protected List<SecondaryNeuron<Food>> foodInputs = new ArrayList<>();
	protected FuelInputNeuron hungerInput = new FuelInputNeuron(this);
	protected List<SecondaryNeuron<Fish>> happinessNeurons = new ArrayList<>();
    protected float cumulativeHappiness = 0;

	protected Set<WorldEntity> collidedEntities = new HashSet<>();
	private Chromosome chromosome;
	private float speed;
	private int age = 0;
	private boolean hasSecondaryNeurons;
	private int fitness = 0;
	private boolean isHidden= false;
	
    protected void createNeuron(int offsetX, int offsetY) {
        IntegerPoint point = new IntegerPoint(offsetX, offsetY);
        InputNeuron neuron = new InputNeuron(this, point);
        neurons.add(new InputNeuronWithPosition(neuron, point));
    }


    public Chromosome getChromosome() {
        return chromosome;
    }

    protected Creature() {

    }

    public List<SecondaryNeuron<Fish>> getFishInputs() {
        return fishInputs;
    }

    public List<SecondaryNeuron<Fish>> getDeadFishInputs() {
        return deadFishInputs;
    }

    public List<SecondaryNeuron<Kraken>> getKrakenInputs() {
        return krakenInputs;
    }

    public List<SecondaryNeuron<Food>> getFoodInputs() {
        return foodInputs;
    }

    public List<SecondaryNeuron<Fish>> getHappinessInputs() {
        return happinessNeurons;
    }

    public FuelInputNeuron getHungerInput() {
        return hungerInput;
    }

    public boolean isAlive() {
        return fuel > 0;
    }

    public boolean looksLeft() {
        return looksLeft;
    }

    public void setLooksLeft(boolean looksLeft) {
        this.looksLeft = looksLeft;
    }

    public Creature(World world, float fuel, float x, float y, float width, float height) {
        super(x, y, width, height);
        this.world = world;
        this.fuel = fuel;
        this.speed = 1;

    }

    public synchronized void addFuel(float amount) {
        this.fuel += amount;
        if (fuel > 100) {
            this.fuel = 100;
        }
    }

    public void swim() {
        // calculates fuel with 'living cost' and current movement
        fuel = fuel - 0.1f - ((Math.abs(vx) + Math.abs(vy)) / 100);
        if (fuel < 0)
            fuel = 0;
        vx += dx;
        vy += dy;
        vx *= 0.9;
        vy *= 0.9;
        // To stop creatures from entering the ground: world.getHeight()-50
        // To stop creatures from leaving the window: world.getWidth()-30
        checkWorldCollision(world.getWidth() - 30, world.getHeight() - 50);
        x += vx;
        y += vy;
        dx = 0;
        dy = 0;
    }

    public void sink() {
        vx = .3f * vx;
        vy = 1;
        swim();
    }

    private boolean doesItCut(final float neuronTopY, final float neuronBottomY, final float neuronLeftX,
                              final float neuronRightX, float topY, float bottomY, float leftX, float rightX) {
        boolean isEnclosed = topY <= neuronTopY && bottomY >= neuronBottomY && leftX <= neuronLeftX
                && rightX >= neuronRightX;
        boolean isEnclosing = topY >= neuronTopY && bottomY <= neuronBottomY && leftX >= neuronLeftX
                && rightX <= neuronRightX;
        if (isEnclosed || isEnclosing)
            return true;
        int cuts = 0;
        // check top vs left and right
        if (inBetween(topY, neuronBottomY, neuronTopY)) {
            cuts += cut1(leftX, rightX, neuronRightX, neuronLeftX);
            if (cuts > 1) {
                return true;
            }
        }
        // check bottom vs left and right
        if (inBetween(bottomY, neuronBottomY, neuronTopY)) {
            cuts += cut1(leftX, rightX, neuronRightX, neuronLeftX);
            if (cuts > 1) {
                return true;
            }
        }
        // check left vs top and bottom
        if (inBetween(leftX, neuronRightX, neuronLeftX)) {
            cuts += cut1(topY, bottomY, neuronBottomY, neuronTopY);
            if (cuts > 1) {
                return true;
            }
        }
        // check right vs top and bottom
        if (inBetween(rightX, neuronRightX, neuronLeftX)) {
            cuts += cut1(topY, bottomY, neuronBottomY, neuronTopY);
            if (cuts > 1) {
                return true;
            }
        }
        return false;
    }

    public void perceive() {
        for (InputNeuronWithPosition inputNeuron : neurons)
            inputNeuron.neuron.clear();
        collidedEntities = new HashSet<>();
        for (WorldEntity entity : world.getEntities()) {
            if (entity != this) { // skip yourself ;)
                boolean collided = doesItCut(getTopY(), getBottomY(), getLeftX(), getRightX(), entity.getTopY(),
                        entity.getBottomY(), entity.getLeftX(), entity.getRightX());
                if (collided)
                    collidedEntities.add(entity);
                for (InputNeuronWithPosition inputNeuron : neurons) {
                    final float neuronTopY = inputNeuron.neuron.getTopY();
                    final float neuronBottomY = inputNeuron.neuron.getBottomY();
                    final float neuronLeftX = inputNeuron.neuron.getLeftX();
                    final float neuronRightX = inputNeuron.neuron.getRightX();
                    boolean cuts = doesItCut(neuronTopY, neuronBottomY, neuronLeftX, neuronRightX, entity.getTopY(),
                            entity.getBottomY(), entity.getLeftX(), entity.getRightX());
                    if (cuts) {
                        inputNeuron.neuron.addPerceivedEntity(entity);
                    }
                }
            }
        }
    }

    private int cut1(float entityLeftX, float entityRightX, float neuronRightX, float neuronLeftX) {
        int cuts = 0;
        if (inBetween(entityLeftX, neuronRightX, neuronLeftX))
            cuts++;
        if (inBetween(entityRightX, neuronRightX, neuronLeftX))
            cuts++;
        return cuts;
    }

    private boolean inBetween(float value, float upperBound, float lowerBound) {
        return value < upperBound && value > lowerBound;
    }

    public void moveN(double factor) {
        dy += -speed*factor;
        this.checkWorldCollision(world.getWidth(), world.getHeight());
        this.looksLeft = true;
    }

    public void moveE(double factor) {
        this.dx += speed*factor;
        this.checkWorldCollision(world.getWidth(), world.getHeight());
        this.looksLeft = false;

    }

    public void moveS(double factor) {
        this.dy += speed*factor;
        this.checkWorldCollision(world.getWidth(), world.getHeight());
        this.looksLeft = false;
    }

    public void moveW(double factor) {
        this.dx += -speed*factor;
        this.checkWorldCollision(world.getWidth(), world.getHeight());
        this.looksLeft = true;
    }

    /**
     * call when all {@link InputNeuron}s have been created
     */
    protected void createSecondaryNeurons() {
        if (!hasSecondaryNeurons) {
            neurons.forEach((inputNeuron) -> {
                krakenInputs.add(new KrakenInputNeuron(inputNeuron.neuron, Kraken.class));
                fishInputs.add(new FishInputNeuron(inputNeuron.neuron, Fish.class));
                deadFishInputs.add(new DeadFishInputNeuron(inputNeuron.neuron, Fish.class));
                foodInputs.add(new FoodInputNeuron(inputNeuron.neuron, Food.class));
            });
            hasSecondaryNeurons = true;
        }
    }

    public void createHappinessNeurons() {
        // override this
    }

    public void determineCurrentHappiness() {
        // override this
    }

    public ArrayList<InputNeuronWithPosition> getNeurons() {
        return neurons;
    }

	public void setFitnessValue(float age) {
	    final float importanceOfSurvival = 2.0f;
	    final float importanceOfHappiness = 1.0f;
		fitness = (int) (age * importanceOfSurvival + importanceOfHappiness*cumulativeHappiness/age);
		chromosome.setFitnessValue(fitness);
	}
	
	public int getFitnessValue() {
		return fitness;
	}

    public void setChromosome(Chromosome chromosome) {
        this.chromosome = chromosome;
    }

    public abstract void handleCollisions();

    public void age() {
        age++;
        determineCurrentHappiness();
    }

    public void onDeath() {
        setFitnessValue(age);
    }

    public void setFuel(float fuel) {
        this.fuel = fuel;
    }

    public float getFuel() {
        return fuel;
    }

    public void reset() {
        fuel = 100f;
        consumed = false;
    }

    public int getAge() {
        return age;
    }
    
    public boolean isHidden() {
		return isHidden;
	}

	public void setHidden(boolean isHidden) {
		this.isHidden = isHidden;
	}

    public static Integer calcInputSize(Creature creature) {
        return creature.fishInputs.size()
              +creature.deadFishInputs.size()
              +creature.foodInputs.size()
              +creature.krakenInputs.size()
              +creature.happinessNeurons.size()
              +1 /*hunger input*/;
    }
}
