Bedürfnisse der Fische werden durch operante Konditionierung umgesetzt. Bestimmtes Verhalten wird belohnt, 
anderes bestraft. Das soll über die Fuel-Funktion realisiert werden.

Existenzerhaltende Bedürfnisse
	- Energie (Hunger)
		Fische bevorzugen bestimmte Futtersorte. Es gibt verschiedene Sorten, während eine Sorte den 
		Fuel-Wert am meisten vergrößert und eine andere sogar giftig sein kann, sprich den Wert verkleinert.
		
Informationsbedürfnisse
	- Affiliation (Zuwendung durch andere Lebewesen)
		Fische halten sich gern beieinander auf, sprich der Fuel-Wert wird erhöht wenn andere Fische in der
		Umgebung sind.
		
	Mit den Generationen wird die Bestimmtheit der Fische, sowie deren Kompetenz zunehmen, da die Netze 
	bereits gelernt haben und so deren Verhalten zielgerichteter und somit vorhersagbarer sein wird.
	
	Ein Modellorganismus kann verschiedene Bedürfnisse, die auch widersprüchlich sein können, haben. Daher 
	geben wir der Umgebung Eigenschaften, die Temperatur, Salinität oder Sauerstoffgehalt des Wassers sein 
	können.
	
Erweiterte Bedürfnisse
	- Wohlbefinden (Zufriedenheit)
		Kraken mögen Temperaturen am Boden nicht, also sind Fische dort hinreichend sicher. Da Futter 
		jedoch von oben regnet, wird es sich für die Fische auch lohnen in den gefährlicheren Bereich 
		zu schwimmen.
	- Emotion (Angst)
		Fische erkennen die Gräten anderer Fische und da das ein Hinweis für eine Gefahrensituation sein kann,
		schwimmen die Fische prinzipiell vor Gräten weg. 